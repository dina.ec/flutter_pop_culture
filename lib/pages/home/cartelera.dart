import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:pop_culture/conts.dart';
import 'package:pop_culture/framework/flutter_dina.dart';
import 'package:pop_culture/models/movie.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:pop_culture/pages/moviePage.dart';
import 'package:pop_culture/pages/movieDetailPage.dart';
import 'dart:math' as math;

class CarteleraPage extends StatefulWidget {
  final DinaFramework fr;

  const CarteleraPage({Key key, @required this.fr}) : super(key: key);

  @override
  _CateleraState createState() => new _CateleraState();
}

class _CateleraState extends State<CarteleraPage> {
  List<Movie> movies = <Movie>[];
  Movie movie;

  ScrollController listController;

  PageController controller;
  int currentpage = 0;
  int indexMovie = 0;

  @override
  initState() {
    super.initState();
    controller = new PageController(
      initialPage: currentpage,
      keepPage: false,
      viewportFraction: 0.5,
    );
    listController = new ScrollController();
    fetchMovies();
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  fetchMovies() async {
    var url =
        'https://api.themoviedb.org/3/movie/now_playing?api_key=$themoviedb&language=es-EC&page=1';
    final response = await http.get(url);

    if (response.statusCode == 200) {
      List<Movie> tmpMovies = <Movie>[];
      var parsed = jsonDecode(response.body);

      for (var movieJSON in parsed['results']) {
        var movie = Movie.fromJson(movieJSON);
        if (movie.backdrop_path != null) {
          tmpMovies.add(Movie.fromJson(movieJSON));
        }
      }
      var movieTmp = tmpMovies[0];
      setState(() {
        movies = tmpMovies;
        movie = movieTmp;
      });
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }

  _build1() {
    return new ListView.builder(
      controller: listController,
      scrollDirection: Axis.vertical,
      itemCount: movies.length,
      itemBuilder: (context, index) {
        final imovie = movies[index];
        return new CupertinoButton(
            padding: EdgeInsets.all(5.0),
            child: new Material(
              borderRadius: BorderRadius.circular(10.0),
              elevation: 4.0,
              shadowColor: Colors.black26,
              child: new Row(
                children: <Widget>[
                  new Stack(
                    children: <Widget>[
                      new ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10.0),
                            bottomLeft: Radius.circular(10.0)),
                        child: new SizedBox(
                          width: widget.fr.wp(30.0),
                          height: widget.fr.hp(24.0),
                          child: Hero(
                              tag: 'movie' + index.toString(),
                              child: Image.network(
                                themoviedb_img + imovie.poster_path,
                                fit: BoxFit.fill,
                              )),
                        ),
                      ),
                      new Destacado(
                        color: index % 2 == 0 ? Colors.red : Colors.amber,
                        text: index % 2 == 0 ? "ESTRENO" : "PRE-VENTA",
                        fontSize: widget.fr.ip(1.0),
                        opacity: 1.0,
                      )
                    ],
                  ),
                  new SizedBox(width: 4.0),
                  new Flexible(
                      child: new Column(
                    children: <Widget>[
                      new Text(imovie.title,
                          style: TextStyle(
                              height: 0.8,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: widget.fr.ip(1.9))),
                      new SizedBox(height: 4.0),
                      new Text("12 años | drama",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: widget.fr.ip(1.2))),
                      new Text("11H00 | 13H30 | 15H00",
                          style: TextStyle(
                              color: Colors.amber,
                              fontSize: widget.fr.ip(1.5),
                              fontWeight: FontWeight.bold)),
                      new SizedBox(height: 4.0),
                      new Text(imovie.overview,
                          maxLines: 5,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              height: 0.8,
                              color: Colors.black54,
                              fontSize: widget.fr.ip(1.3))),
                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ))
                ],
              ),
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => new MovieDetailPage(
                          movie: imovie,
                          fr: widget.fr,
                          tag: 'movie' + index.toString())));
            });
      },
    );
  }

  _pageViewMovies() {
    return new Container(
      height: widget.fr.hp(35.0),
      child: new PageView.builder(
          onPageChanged: (value) {
            setState(() {
              currentpage = value;
            });
          },
          controller: controller,
          itemBuilder: (context, index) => builder(index)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: movies.length > 0
          ? _build1()
          : Center(
              child: SpinKitDualRing(
                color: Colors.red,
                size: 80.0,
              ),
            ),
    );
  }

  builder(int index) {
    var movie = movies[index];
    return new AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        double value = 1.0;
        if (controller.position.haveDimensions) {
          value = controller.page - index;
          value = (1 - (value.abs() * .4)).clamp(0.0, 1.0);
        }

        return new Center(
          child: new SizedBox(
            height: Curves.easeOut.transform(value) * widget.fr.hp(50.0),
            width: Curves.easeOut.transform(value) * widget.fr.wp(53.0),
            child: child,
          ),
        );
      },
      child: new CupertinoButton(
          padding: EdgeInsets.all(1.0),
          child: new ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: Image.network(themoviedb_img + movie.poster_path),
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => new MoviePage(
                          movieId: movie.id,
                        )));
          }),
    );
  }
}

class Destacado extends StatelessWidget {
  double opacity = 1.0;
  double fontSize = 10.0;
  Color color = Colors.amber;
  String text = "";

  Destacado({Key key, this.opacity, this.color, this.text, this.fontSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Positioned(
        top: MediaQuery.of(context).size.height * (2 / 100),
        right: -MediaQuery.of(context).size.width * (7 / 100),
        child: new Transform.rotate(
          angle: 45 * math.pi / 180,
          child: new Opacity(
            opacity: opacity,
            child: new Container(
              width: MediaQuery.of(context).size.width * (28 / 100),
              height: MediaQuery.of(context).size.width * (5 / 100),
              padding: EdgeInsets.only(
                  top: 3.0, bottom: 3.0, left: 10.0, right: 10.0),
              color: color,
              child: Text(
                this.text,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: fontSize,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ));
  }
}
