import 'package:flutter/material.dart';
import 'dart:math' as math;

class DinaFramework {
  final BuildContext context;
  double width;
  double height;
  double inch_screen;

  DinaFramework.init(this.context) {
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    inch_screen = math.sqrt(math.pow(width, 2) + math.pow(height, 2));
  }

  double wp(double porcentaje) {
    return width * (porcentaje / 100);
  }

  double hp(double porcentaje) {
    return height * (porcentaje / 100);
  }


  double ip(double porcentaje) {
    return inch_screen * (porcentaje / 100);
  }
}
